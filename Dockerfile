FROM node:lts-alpine3.17

WORKDIR /belajar-unit-testing

COPY package*.json ./

COPY . .

EXPOSE 8090

CMD [ "npm", "run", "start" ]